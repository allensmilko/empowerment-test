import type { AWS } from '@serverless/typescript';

import movies from '@functions/movies';
import {  newFavorite, getMyFavorites } from '@functions/favorites';
import {  newNote, getMyNotes } from '@functions/notes';

const secretsManagerArn = "arn:aws:secretsmanager:us-east-1:173000104414:secret:movies-api-secrets-uZ8eN7";
const serverlessConfiguration: AWS = {
  service: 'empowerment-test',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild', 'serverless-offline'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
    iam:{
      role:{
        statements: [
          {
            Effect: 'Allow',
            Action: 'secretsmanager:GetSecretValue',
            Resource: secretsManagerArn
          }
        ]
      }
    }
  },
  functions: { 
    movies,
    newFavorite,
    getMyFavorites,
    newNote,
    getMyNotes
   },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
};

module.exports = serverlessConfiguration;
