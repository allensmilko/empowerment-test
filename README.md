## Installation/deployment instructions

### Using NPM

- Run `npm i` to install the project dependencies
- Run `npx sls deploy` to deploy this stack to AWS

### Using Yarn

- Run `yarn` to install the project dependencies
- Run `yarn sls deploy` to deploy this stack to AWS

## Test your service

Actually, the main url of the deployed project is `https://k3iyldtxlh.execute-api.us-east-1.amazonaws.com/dev/`

This projects contains multiples lambda function triggered by an HTTP request made on the provisioned API Gateway REST API `/movie` `/favorite` and `/note` route with `POST` method. The request body must be provided as `application/json`. The body structure is tested by API Gateway against `src/dtos` JSON-Schema.

### Locally

In order to test some function locally, run the following command:

- `npx sls invoke local -f some --path jsonmockfilepath.json` if you're using NPM
- `yarn sls invoke local -f some --path jsonmockfilepath.json` if you're using Yarn

Check the [sls invoke local command documentation](https://www.serverless.com/framework/docs/providers/aws/cli-reference/invoke-local/) for more information.

### Remotely

Copy and replace your `url` - found in Serverless `deploy` command output - and `name` parameter in the following `curl` command in your terminal or in Postman to test your newly deployed application.

```
curl --location --request POST 'https://myApiEndpoint/dev/some' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Frederic"
}'
```


### Project structure

The project code base is mainly located within the `src` folder. This folder is divided in:

- `functions` - containing code base and configuration for your lambda functions
- `libs` - containing shared code base between your lambdas

```
.
├── src
|   |__ models                  # Domain models
|   |__ dtos                    # Data transfer objects
│   ├── functions               # Lambda configuration and source code folder
│   │   ├── some
│   │   │   ├── handler.ts      # `some` lambda source code
│   │   │   ├── index.ts        # `some` lambda Serverless configuration
│   │   │   ├── mock.json       # `some` lambda input parameter, if any, for local invocation
│   │   │
│   │   │
│   │   └── index.ts            # Import/export of all lambda configurations
│   │
│   └── libs                    # Lambda shared code
│       └── apiGateway.ts       # API Gateway specific helpers
│       └── handlerResolver.ts  # Sharable library for resolving lambda handlers
│       └── lambda.ts           # Lambda middleware
│       └── some libraries with functionality like: connect to database, and calls 3parthy
│
├── package.json
├── serverless.ts               # Serverless service file
├── tsconfig.json               # Typescript compiler configuration
├── tsconfig.paths.json         # Typescript paths
└── webpack.config.js           # Webpack configuration
```
