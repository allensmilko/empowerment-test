import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import MovieService from "./service";
const movieService = new MovieService();

const getPopular = async () => {
  const response = await movieService.getPopular();
  return formatJSONResponse(response.status, response);
};

export const popular = middyfy(getPopular);
