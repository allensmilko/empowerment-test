// import { NewFavorite } from '@dtos/Favorite';
import { handlerPath } from '@libs/handler-resolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.popular`,
  events: [
    {
      http: {
        method: 'get',
        path: 'popular'
      },
    }
  ],
};
