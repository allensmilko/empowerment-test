import { getConnection } from '@libs/db';
import { Response } from '@dtos/Api';
import { Movie } from '@dtos/Movie';
import { MoviesRequest } from '@libs/movies-api';
import MovieModel from '@models/Movie';

class MovieService {

  async getPopular(): Promise<Response> {
    try {
      const movieModel = (await getConnection()).model('Movie', MovieModel.schema);
      const currentDate = new Date();
      let localMovies = await movieModel.find({});
      
      if(!localMovies.length) {
        const getPopular = await MoviesRequest('movie/popular');
        if(!getPopular.results){
          return getPopular;
        }
        const expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 1);
  
        getPopular.results.forEach((currentMovie: Movie) => {
          currentMovie.expire_at = expireDate;
        });
  
        localMovies = await movieModel.insertMany(getPopular.results);
      }

      if(localMovies[0].expire_at > currentDate){
        await movieModel.remove();
        const getPopular = await MoviesRequest('movie/popular');
        if(!getPopular.results){
          return getPopular;
        }
        const expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 1);
  
        getPopular.results.forEach((currentMovie: Movie) => {
          currentMovie.expire_at = expireDate;
        });
  
        await movieModel.insertMany(getPopular.results);
      }

      return {
        status: 200,
        results: localMovies
      };
   
    } catch (error) {
      return error;
    }
  }
}

export default MovieService;
