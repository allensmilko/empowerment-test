import movies from './movies';
import { newFavorite, getMyFavorites } from './favorites';
import { newNote, getMyNotes } from './notes';

export {
    movies,
    newFavorite,
    getMyFavorites,
    newNote,
    getMyNotes
};