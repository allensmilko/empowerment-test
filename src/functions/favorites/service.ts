import { getConnection } from '@libs/db';
import { Response } from '@dtos/Api';
import { Favorite } from '@dtos/Favorite';
import FavoriteModel from '@models/Favorite';

class FavoriteService {

  async newFavorite(data: any): Promise<Response> {
    try {
      const favoriteModel = (await getConnection()).model('Favorite', FavoriteModel.schema);
      const newFavorite = new favoriteModel(data);
      const created = await newFavorite.save();
       return {
        status: 200,
        data: created
       }
    } catch (error) {
      return {
        status: 200,
        data: error
       }
    }
  }

  async getMyfavorites(): Promise<Response> {
    try {
      const conn = await getConnection();
      const favoriteModel = await conn.model('Favorite', FavoriteModel.schema);

      const finded: Favorite[] = await favoriteModel.find({});
       return {
        status: 200,
        data: finded
       }
    } catch (error) {
      return {
        status: 200,
        data: error
       }
    }
  }
}

export default FavoriteService;
