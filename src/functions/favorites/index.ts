import { handlerPath } from '@libs/handler-resolver';
import { newFavoriteRequestSchema } from '@dtos/Favorite';

export const newFavorite = {
  handler: `${handlerPath(__dirname)}/handler.newFavorite`,
  events: [
    {
      http: {
        method: 'post',
        path: 'favorite',
        request: {
          schemas: {
            'application/json': newFavoriteRequestSchema,
          },
        },
      },
    },
  ]
};

export const getMyFavorites = {
  handler: `${handlerPath(__dirname)}/handler.myfavorites`,
  events: [
    {
      http: {
        method: 'get',
        path: 'my-favorites'
      },
    },
  ]
};
