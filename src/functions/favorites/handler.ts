import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import FavoriteService from "./service";
import { newFavoriteRequestSchema } from '@dtos/Favorite';

const favoriteService = new FavoriteService();

const createNewFavorite: ValidatedEventAPIGatewayProxyEvent<typeof newFavoriteRequestSchema> = async (event) => {
  const response = await favoriteService.newFavorite(event.body);
  return formatJSONResponse(response.status, response);
};

export const newFavorite = middyfy(createNewFavorite);
const getMyfavorites = async () => {
  const response = await favoriteService.getMyfavorites();
  return formatJSONResponse(response.status, response);
};

export const myfavorites = middyfy(getMyfavorites);

