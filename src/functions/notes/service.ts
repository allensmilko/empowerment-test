import { getConnection } from '@libs/db';
import { Response } from '@dtos/Api';
import { Note } from '@dtos/Note';
import NoteModel from '@models/Note';

class NoteService {

  async newNote(data: any): Promise<Response> {
    try {
      const noteModel = (await getConnection()).model('Note', NoteModel.schema);
      const newNoteModel = new noteModel(data);
      const created = await newNoteModel.save();
       return {
        status: 200,
        data: created
       }
    } catch (error) {
      return {
        status: 200,
        data: error
       }
    }
  }

  async getMyNotes(): Promise<Response> {
    try {
      const conn = await getConnection();
      const noteModel = await conn.model('Note', NoteModel.schema);

      const finded: Note[] = await noteModel.find({})
       return {
        status: 200,
        data: finded
       }
    } catch (error) {
      return {
        status: 200,
        data: error
       }
    }
  }
}

export default NoteService;
