import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import NoteService from "./service";
import { newNoteRequestSchema } from '@dtos/Note';
const noteService = new NoteService();

const createNote: ValidatedEventAPIGatewayProxyEvent<typeof newNoteRequestSchema> = async (event) => {
  const response = await noteService.newNote(event.body);
  return formatJSONResponse(response.status, response);
};

export const newNote = middyfy(createNote);

const getMyNotes = async () => {
  const response = await noteService.getMyNotes();
  return formatJSONResponse(response.status, response);
};

export const myNotes = middyfy(getMyNotes);
