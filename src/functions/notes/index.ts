import { handlerPath } from '@libs/handler-resolver';
import { newNoteRequestSchema } from '@dtos/Note';

export const newNote = {
  handler: `${handlerPath(__dirname)}/handler.newNote`,
  events: [
    {
      http: {
        method: 'post',
        path: 'note',
        request: {
          schemas: {
            'application/json': newNoteRequestSchema,
          },
        },
      },
    },
  ]
};

export const getMyNotes = {
  handler: `${handlerPath(__dirname)}/handler.myNotes`,
  events: [
    {
      http: {
        method: 'get',
        path: 'my-notes',
      },
    },
  ]
};
