import { Document } from 'mongoose';
import { float } from "aws-sdk/clients/lightsail";

export interface Movie extends Document{
    adult: boolean,
    backdrop_path: string,
    genre_ids: number[],
    id: number,
    original_language: string,
    original_title: string,
    overview: string,
    popularity: float,
    poster_path: string,
    release_date: string,
    title: string,
    video: boolean,
    vote_average: float,
    vote_count: number,
    expire_at: Date
}