import mongoose, { Document } from 'mongoose';
import { Movie } from './Movie';

export interface NewFavorite extends Document{
    user_id?: string,
    movie_id?: Movie,
}

export interface Favorite extends Document{
  user_id: string,
  movie_id: Movie,
}

export const newFavoriteRequestSchema = {
    type: "object",
    properties: {
      user_id: { type: mongoose.Types.ObjectId },
      movie_id: { type: mongoose.Types.ObjectId },
    },
    required: ['movie_id']
  } as const;
  