import { Document } from "mongoose";

export interface NewUser extends Document {
  email?: string;
  password?: string;
  comparePassword: (password: string) => Promise<Boolean>;
}

export interface User extends Document {
  email: string;
  password: string;
  comparePassword: (password: string) => Promise<Boolean>;
}
