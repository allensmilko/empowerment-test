import { Movie } from "./movie";

export interface Paginate{
    status?: number;
    page?: number;
    results?: Movie[];
    data?: DataError;
    total_pages?: number;
    total_results?: number;
};

export interface DataError {
    status_code: number;
    succeed: boolean;
    status_message: string;
}

export interface Error {
    status: number;
    data: DataError;
}

