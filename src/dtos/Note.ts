import mongoose, { Document } from 'mongoose';
import { Movie } from './Movie';

export interface NewNote extends Document{
    user_id?: string,
    movie_id?: Movie,
    note_title?: string,
    description?: string,
}

export interface Note extends Document{
  user_id: string,
  movie_id: Movie,
  note_title: string,
  description: string,
}

export const newNoteRequestSchema = {
    type: "object",
    properties: {
      user_id: { type: mongoose.Types.ObjectId },
      note_title: { type: String },
      description: { type: String },
      movie_id: { type: mongoose.Types.ObjectId },
    },
    required: ['movie_id']
  } as const;
  