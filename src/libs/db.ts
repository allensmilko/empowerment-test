import mongoose from "mongoose";
import { GetSecrets } from './secrets-manager';
let conn: mongoose.Connection = null;

export const getConnection = async (): Promise<mongoose.Connection> => {
  if (conn == null) {
    const { MONGO_DEB_URL } = await GetSecrets();

    conn = await mongoose.createConnection(MONGO_DEB_URL, {});
  }

  return conn;
};