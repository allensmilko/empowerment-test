
import {
  SecretsManagerClient,
  GetSecretValueCommand,
} from "@aws-sdk/client-secrets-manager";

const secretName = "movies-api-secrets";

const client = new SecretsManagerClient({
  region: "us-east-1",
});

export const GetSecrets = async() => {
  let response;

  try {
    response = await client.send(
      new GetSecretValueCommand({
        SecretId: secretName,
        VersionStage: "AWSCURRENT",
      })
    );
  } catch (error) {
    throw error;
  }
  
  return JSON.parse(response.SecretString);
}
