import { Request } from 'express';
import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import { User } from '@dtos/User';
import UserModel from '@models/User';

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'sdfweEmpower!',
  passReqToCallback: true
};

export default new Strategy(opts, async function(req:Request, payload:any, done:any){
  try {
    const user: User | null = await UserModel.findById(payload._id);
    if(user){
      req.user = user._id;
      return done(null, user);
    }
    return done(null, false);
  } catch (error) {
    console.log(error);
  }
});