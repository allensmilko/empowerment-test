import axios from 'axios';
import { Paginate } from '@dtos/MoviesApi';
import { GetSecrets } from './secrets-manager';
const api = 'https://api.themoviedb.org/3';

export const MoviesRequest = async(path: string): Promise<Paginate> => {
    try {
      const { MOVIES_ACCESS_TOKEN } = await GetSecrets();
      const {data} = await axios.get<Paginate>(
        `${api}/${path}`,
        {
          headers: {
            Accept: 'application/json',
            'accept-encoding': 'application/json',
            'Authorization': `Bearer ${MOVIES_ACCESS_TOKEN}`
          },
        },
      );
      return data;
    } catch (error) {
      return {
        status: 500,
        data: {
          status_code: 500,
          status_message: "unexpected error",
          succeed: false
        }
      };
    }
  }