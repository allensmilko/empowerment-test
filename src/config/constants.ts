export const constants = {
  stage: process.env.STAGE || 'main',
  cors: process.env.CORS,
}