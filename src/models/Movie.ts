import { model,  Schema } from 'mongoose';
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";
import { Movie } from '@dtos/Movie';

const movieSchema = new Schema({
    adult: { type: Boolean },
    backdrop_path: { type: String },
    genre_ids: [{ type: Number }],
    movieId: { type: Number },
    id: { type: Number, unique: true },
    original_language: { type: String },
    original_title: { type: String },
    overview: { type: String },
    popularity: { type: Number },
    poster_path: { type: String },
    release_date: { type: String },
    title: { type: String },
    video: { type: Boolean },
    vote_average: { type: Number },
    vote_count: { type: Number },
    expire_at: { type: Date },
}, {
    versionKey: false,
    timestamps: true
});
movieSchema.plugin(mongoosePagination);

const MovieModel: Pagination<Movie> = model<Movie, Pagination<Movie>>('Movie', movieSchema);

export default MovieModel;
