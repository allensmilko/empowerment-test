import { model,  Schema } from 'mongoose';
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";
import { Favorite } from '@dtos/Favorite';

const favoriteSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    movie_id: { type: Schema.Types.ObjectId, ref: 'Movie' },
}, {
    versionKey: false,
    timestamps: true
});
favoriteSchema.plugin(mongoosePagination);

const FavoriteModel: Pagination<Favorite> = model<Favorite, Pagination<Favorite>>('Favorite', favoriteSchema);

export default FavoriteModel;
