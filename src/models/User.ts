import bcrypt from "bcrypt";
import { Schema, model } from "mongoose";
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { User } from '@dtos/User';

const userSchema = new Schema<User>(
  {
    email: { type: String, unique: true },
    password: { type: String },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

userSchema.pre<User>("save", async function (next) {
  const user = this;
  if (!user.isModified("password")) return next();
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(user.password, salt);
  user.password = hash;
  next();
});

userSchema.methods.comparePassword = async function (
  password: string
): Promise<Boolean> {
  return await bcrypt.compare(password, this.password);
};

userSchema.plugin(mongoosePagination);
userSchema.plugin(aggregatePaginate);

const UserModel: Pagination<User> = model<User, Pagination<User>>('User', userSchema);
export default UserModel;
