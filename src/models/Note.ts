import { model,  Schema } from 'mongoose';
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";
import { Note } from '@dtos/Note';

const noteSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    movie_id: { type: Schema.Types.ObjectId, ref: 'Movie' },
    note_title: { type: String },
    description: { type: String },
}, {
    versionKey: false,
    timestamps: true
});
noteSchema.plugin(mongoosePagination);

const NoteModel: Pagination<Note> = model<Note, Pagination<Note>>('Note', noteSchema);

export default NoteModel;
